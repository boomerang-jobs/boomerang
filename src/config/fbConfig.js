import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyB7d6ypyOi12wtt8nwCxdv7875yakdIo04",
  authDomain: "boomerang-55061.firebaseapp.com",
  databaseURL: "https://boomerang-55061.firebaseio.com",
  projectId: "boomerang-55061",
  storageBucket: "boomerang-55061.appspot.com",
  messagingSenderId: "1094615000799",
  appId: "1:1094615000799:web:000c260583a184289eab6a",
  measurementId: "G-BZK0MPZEQZ"
};

firebase.initializeApp(config);
// firebase.firestore(); // We don't need this anymore because we create firestore in index.js

export default firebase;
