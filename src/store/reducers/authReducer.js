const initState = {};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case 'LOGIN_ERROR':
      return {
        ...state,
        authErrorLogin: action.err.message,
      };
    case 'LOGIN_SUCCESS':
      console.log('login success');
      return {
        ...state,
        authErrorLogin: null,
      };
    case 'SIGNOUT_SUCCESS':
      console.log('signout success');
      return state;
    case 'SIGNUP_SUCCESS':
      console.log('signup success');
      return {
        ...state,
        authErrorRegister: null,
      };
    case 'SIGNUP_ERROR':
      console.log('signup failed');
      return {
        ...state,
        authErrorRegister: action.err.message,
      };
    default:
      return state;
  }
};

export default authReducer;
