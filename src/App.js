import React, { Component } from 'react';
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import bootstrap from 'react-bootstrap';

import Feed from './components/feed/Feed';
import ResumeDetails from './components/resumes/ResumeDetails';
import SignInPage from './pages/SignIn/SignIn';
import RegisterPage from './pages/Register/Register';
import ResumeBuilder from './components/resumes/ResumeBuilder';

class App extends Component {
  render() {
    const { auth } = this.props;

    // if auth is loaded then we render App.
    // But if not then we doesn't render the one.
    if (auth.isLoaded) {
      return (
        <BrowserRouter>
          <div className="App">
            <Switch>
              <Route exact path="/feed" component={Feed} />
              <Route path="/resume/:id" component={ResumeDetails} />
              <Route path="/signin" component={SignInPage} />
              <Route path="/register" component={RegisterPage} />
              <Route path="/resume" component={ResumeBuilder} />
              <Redirect path='/' to='/signin' />
            </Switch>
          </div>
        </BrowserRouter>
      );
    }

    return null;
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth,
});

export default compose(
  firebaseConnect(),
  connect(mapStateToProps),
)(App);
