import React from 'react';
import { Link } from 'react-router-dom';

import ResumeThumb from './ResumeThumb';

const ResumeItem = ({ Resumes }) => (
  <div>
    <Link className='card mb-3' to='/resume'>
      <div className='card-body'>
        <div className='mt-1'> Create a Post </div>
      </div>
    </Link>
    {Resumes && Resumes.map(resume => (
      <Link to={`/resume/${resume.id}`} key={resume.id}>
        <ResumeThumb resume={resume} />
      </Link>
    ))}
  </div>
);

export default ResumeItem;
