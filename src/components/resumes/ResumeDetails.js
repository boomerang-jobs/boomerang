import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import moment from 'moment';

import Navbar from '../header/Navbar';

const ResumeDetails = (props) => {
  const { auth } = props;
  if (!auth.uid) {
    return <Redirect to='/signin' />;
  }

  const { resume } = props;
  if (resume) {
    return (
      <div>
        <Navbar />
        <div className='container mt-4'>
          <div className='row'>
            <div className='col-md-8 offset-md-2'>
            <Link to='/feed'><small>Back to the feed</small></Link>
              <div className='card'>
                <div className='card-body'>
                  <h4 className='card-title vollkorn mb-3'>{resume.resumeTitle}</h4>
                  <small className='text-muted'>Story</small>
                  <p className='mb-4'>{resume.story}</p>
                  <small className='text-muted'>The Ask</small>
                  <p>{resume.ask}</p>
                  <div className='btn btn-sm btn-dark mt-4 mb-3'> Private Message </div>
                  <div className='btn btn-sm btn-outline-dark ml-2 mt-4 mb-3'>Report</div>
                </div>
              <div className='card-footer'>
                <small>{moment(resume.createdAt.toDate()).calendar()}</small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }

  return (
    <div className='container center'>
      <p>Loading resume...</p>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params;
  const { Resumes } = state.firestore.data;
  const resume = Resumes ? Resumes[id] : null;

  return {
    resume,
    auth: state.firebase.auth,
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'Resumes' },
  ]),
)(ResumeDetails);
