import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import createResume from '../../store/actions/resumeActions';

import Navbar from '../header/Navbar';

class ResumeBuilder extends Component {
  state = {
    resumeTitle: '',
    story: '',
    ask: ''
  }

  handleChange = (e) => {
    const { target } = e;

    this.setState(state => ({
      ...state,
      [target.id]: target.value,
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    const { props, state } = this;
    props.createResume(state);

    props.history.push('/feed');
  }

  render() {
    const { auth } = this.props;
    if (!auth.uid) {
      return <Redirect to="/signin" />;
    }

    return (
      <div>
      <Navbar />
        <div className='container'>
          <div className='row mt-5'>
            <div className='col-md-8 offset-md-2'>
              <form onSubmit={this.handleSubmit} className='mb-4'>
              <div className='form-group col-md-8 p-0 pr-2'>
                <label htmlFor="resumeTitle">Post Title</label>
                <input className="form-control" type="text" maxlength='100' name="resumeTitle" required id="resumeTitle" onChange={this.handleChange} />
              </div>
                <label htmlFor="story">Story</label>
                <textarea name="story" id="story" maxlength='500' cols="10" rows="3" className="form-control" onChange={this.handleChange} />
                <div className='mb-4'><small className='text-muted'>450 characters or less</small></div>

                <label htmlFor="ask">Ask</label>
                <textarea name="ask" id="ask" maxlength='140' cols="10" rows="3" required className="form-control" onChange={this.handleChange} />
                <div className='mb-4'><small className='text-muted'>160 characters or less</small></div>

                <button type="submit" className="btn btn-dark mb-2">Create</button>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth,
});

const mapDispatchToProps = dispatch => ({
  createResume: resume => dispatch(createResume(resume)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ResumeBuilder);
