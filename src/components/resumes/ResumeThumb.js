import React from 'react';
import moment from 'moment';

const ResumeThumb = ({ resume }) => (
  <div className='card mb-3'>
    <div className='card-body'>
      <h4 className='card-title text-dark vollkorn'>{resume.resumeTitle}</h4>
        <div className='card-text mt-3'>
          <small className='text-muted'>The Ask</small>
          <p>{resume.ask}</p>
        <div className='text-muted mt-2'>
          <small>
            Created &nbsp;
            {moment(resume.createdAt.toDate()).calendar()}
          </small>
        </div>
      </div>
    </div>
  </div>
);

export default ResumeThumb;
