import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Redirect, Link } from 'react-router-dom';
import ResumeItem from '../resumes/ResumeItem';
import Navbar from '../header/Navbar';

class Dashboard extends Component {
  render() {
    // console.log(this.props);
    const { Resumes, auth } = this.props;
    if (!auth.uid) {
      return <Redirect to="/signin" />;
    }

    return (
      <div>
      <Navbar />
        <div className='container'>
          <div className='row mt-4'>
            <div className='col-md-6 offset-md-3'>
              <ResumeItem Resumes={Resumes} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    Resumes: state.firestore.ordered.Resumes,
    auth: state.firebase.auth,
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'Resumes', orderBy: ['createdAt', 'desc'] },
  ]),
)(Dashboard);
